import {Routes,RouterModule}  from "@angular/router";
import {ModuleWithProviders} from "@angular/core";
import {RegistroComponent} from "./components/registro/registro.component";
import {LoginComponent} from "./components/login/login.component";
import { MessangerComponent } from './components/messanger/messanger.component';
import { PerfilComponent } from './components/perfil/perfil.component';

const appRouter : Routes=[
    {path: 'registro' , component:RegistroComponent},
    {path: '' , component:LoginComponent},
    {path : 'messenger',component:MessangerComponent},
    {path : 'perfil',component:PerfilComponent}
]

export const appRoutingProviders : any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRouter);
