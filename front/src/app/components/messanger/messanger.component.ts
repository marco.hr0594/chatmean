import { Component, OnInit, ViewChild, ElementRef, AfterViewChecked } from '@angular/core';
import { UserService } from "../../service/user.service";
import { GLOBAL } from "../../service/Global";
import { Message } from "../../models/Message";

import  * as io from "socket.io-client";
import { Router } from '@angular/router';

import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";
import Push from "push.js";

import * as moment from 'moment';

@Component({
  selector: 'app-messanger',
  templateUrl: './messanger.component.html',
  styleUrls: ['./messanger.component.css']
})
export class MessangerComponent implements OnInit {
  @ViewChild('scrollMe',{static:false}) private myScrollContainer : ElementRef;


  public usuarios;
  public url;
  public user_select;
  public mensajes;

  public identity;
  public token;
  public de;

  public data_msm;
  public send_message;

  public usuarios_activos;

  public socket = io('http://localhost:4201');

  public dates;

  constructor(
    private _userService : UserService,
    private _route : Router,



  ) { 
    this.url = GLOBAL.url;
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.de = this.identity._id;
    this.socket.emit('users-online');
    //console.log(this.usuarios_activos)
  }

  ngOnInit() {
    /*let date = moment();
    console.log(date);*/


    if(this.identity){
      this.data_msm = new Message('','','','');
      this._userService.get_users().subscribe(
      (response:any) =>{
        //console.log(response);
        this.usuarios = response.users;
      });

      this.socket.on('new-message',function(data){
        //console.log(data);
        var data_all = {
          de:data.message.de,
          para:data.message.para,
          msg : data.message.msg,
          createAt: data.message.createAt
        }

        this._userService.get_user(data.message.de).subscribe(
          response=>{
              if(response.user._id != this.de){
                //console.log("Notificación");
                Push.create(response.user.nombre, {
                  body: data.message.msg,
                  icon:this.url+'usuario/img/'+response.user.imagen,
                  timeout: 4000,
                  requireInteraction:true,
                  onClick: function () {
                      window.focus();
                      this.close();
                  }
                });
                (document.getElementById("player") as any).load();
                (document.getElementById("player") as any).play();
              }
          },error =>{

          }
        )

        //console.log(data_all);
        this.mensajes.push(data_all);
      }.bind(this));

      this.socket.on('new-users',function(data){
        //console.log(data);
        this.usuarios = data.users;
      }.bind(this));

      this.socket.on('all-users',function(){
        this._userService.get_all_users().subscribe(
          respose => {
            this.usuarios_activos = respose.total;
          },error => {
            console.log("0 conectados 0 Error en el APi");
          }
        )
      }.bind(this));


    }else{
      this._route.navigate(['/'])
    }
  }

  scrollToBotom():void{
    try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch (err) {
      
    }
  }

  ngAfterViewChecked(): void {
    //Called after every check of the component's or directive's content.
    //Add 'implements AfterContentChecked' to the class.
   this.scrollToBotom(); 
  }

  listar(id){
    this._userService.get_user(id).subscribe(
      response =>{
        //console.log(id);
        
        this.user_select = response.user;
        this._userService.get_messages(this.de,id).subscribe(
          response =>{
            console.log(response); 
            this.mensajes  = response.message;
          },error => {

          }
        )
        
      },error =>{

      }
    )
  }

  onSubmit(msmForm){
    //console.log(msmForm.value);
    if(msmForm.valid){
      this.send_message = {
        de: this.de,
        para:this.user_select._id,
        msg:this.data_msm.msm
      }

      this._userService.get_send_msm(this.send_message).subscribe(
        response =>{
          //console.log(response)
          //socket
          this.socket.emit('save-message',response.message);
          this.data_msm.msm = "";
          this.scrollToBotom(); 

        },error => {

        }
      )
    }
  }

  logout(){
    this._userService.desactivar(this.de).subscribe(
      response =>{
        this._userService.get_users().subscribe(
          response =>{
            this.usuarios = response.users;
            this.socket.emit('save-users',this.usuarios),
            this.socket.emit('users-online');
          }
        )
      },error=>{

      }
    )


    localStorage.removeItem('token');
    localStorage.removeItem('identity');

    this.identity = "";
    this.token = "";

    this._route.navigate(['']);
  }
}
