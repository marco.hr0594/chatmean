import { Component, OnInit } from '@angular/core';
import { User } from "../../models/User";
import { UserService } from "../../service/user.service";
import { Router } from '@angular/router';
import  * as io from "socket.io-client";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public socket = io("http://localhost:4201");

  public user;
  public token;
  public indentity;
  public data_user;
  public usuarios;

  constructor(
    private _userService : UserService,
    private _router : Router
  ) { 
    this.data_user = this._userService.getIdentity();
  }

  ngOnInit() {
    this.user = new User('','','','','','','','','',false);
    if(this.data_user){
      this._router.navigate(['messenger'])
    }
  }
  onSubmit(loginForm){
    //console.log(loginForm.value);
    if(loginForm.valid){
      this._userService.login(this.user).subscribe(
        response =>{
          this.token = response.jwt;
          this.indentity = JSON.stringify(response.user);
          localStorage.setItem('token',this.token);

          this._userService.login(this.user,true).subscribe(
            response =>{
              localStorage.setItem('identity',this.indentity);
              this._userService.activar(response.user._id).subscribe(
                response =>{
                  this._userService.get_users().subscribe(
                    response =>{
                      this.usuarios = response.users;
                      this.socket.emit('save-users',this.usuarios);
                      //Notificación y actualiza num de usuarios en linea
                      (document.getElementById("new-user") as any).load();
                      (document.getElementById("new-user") as any).play();
                      this.socket.emit('users-online');
                    },error =>{
                      
                    }
                  )
                },error => {

                }
              )
              this._router.navigate(['messenger'])
            },error=>{

            }
          )
          //console.log(response);
          //this.token = response
        },error => {

        }
      )
    }else{
      console.log("Completar campos")
    }
  }

}
