import { Component, OnInit } from '@angular/core';
import  * as io from "socket.io-client";
import { GLOBAL } from "../../service/GLOBAL";
import { UserService } from "../../service/user.service";
import { Router } from '@angular/router';

interface HTMLInputEvent extends Event {
  target: HTMLInputElement & EventTarget;
}

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  public socket = io("http://localhost:4201");
  public identity;
  public url;
  public de;
  public datos_config : any = {};
  public datos_user : any = {};
  public data : any = {};
  public password;
  public confirm_password;
  public msm_confirm_pass;
  public msm_error;
  public usuarios;
  public data_send : any = {};
  public msm_success;

  public passwordText;
  public confirm_passwordText;


  public file : File;
  public imgselected : String | ArrayBuffer;

  constructor(
    private _userService : UserService,
    private _router : Router,

  ) {
    this.url = GLOBAL.url;
    this.identity = this._userService.getIdentity();
    this.de = this.identity._id;
   }

  ngOnInit() {
    if(this.identity){
      this._userService.get_user(this.de).subscribe(
        response =>{
          this.datos_user = response.user;
          //console.log(this.datos_user);
          this.data = {
            nombre : this.datos_user.nombre,
            email : this.datos_user.email,
            telefono : this.datos_user.telefono,
            twitter : this.datos_user.twitter,
            facebook : this.datos_user.facebook,
            bio : this.datos_user.bio,
            github: this.datos_user.github,
            estado : this.datos_user.estado
          }
          //console.log(this.data);
        },error => {

        }
      )
    }else{
      this._router.navigate(['']);
    }
  }

  imgSelected(event:HTMLInputEvent){
    if(event.target.files && event.target.files[0]){
      this.file = <File>event.target.files[0];

      const reader = new FileReader();
      reader.onload = e => this.imgselected = reader.result;
      reader.readAsDataURL(this.file);
    }
  }

  onSubmit(configForm){
    //console.log(configForm.value)
    if(configForm.valid){
      if(configForm.value.password != undefined){
        if(configForm.value.password != configForm.value.confirm_password){
          this.msm_error = "las contraseñas no coinciden"
        }else{
          //las contraseñas son identicas
          this.msm_error ="";
          this.data_send = {
            _id: this.datos_user._id,
            nombre : configForm.value.nombre,
            telefono : configForm.value.telefono,
            imagen :this.file,
            password : configForm.value.password,
            bio : configForm.value.bio,
            facebook : configForm.value.facebook,
            github : configForm.value.github,
            estado : configForm.value.estado,
            twitter : configForm.value.twitter,

          }

          this.socket.emit('save-user',{identity:this.data_send});
          this._userService.update_config(this.data_send).subscribe(
            response=>{
              this.msm_success ="Se actualizaron tus datos con éxito";
              
              this._userService.get_users().subscribe(
                response =>{
                  this.usuarios = response.users;
                  console.log(this.usuarios);
                  this.socket.emit("save-users",this.usuarios)
                },error =>{

                }
              )
              
            },error => {

            }
          )
        }
      }else{
          //las contraseñas son identicas
          this.msm_error ="";
          this.data_send = {
            _id: this.datos_user._id,
            nombre : configForm.value.nombre,
            telefono : configForm.value.telefono,
            imagen :this.file,
            bio : configForm.value.bio,
            facebook : configForm.value.facebook,
            github : configForm.value.github,
            estado : configForm.value.estado,

          }

          this.socket.emit('save-user',{identity:this.data_send});

          this._userService.update_config(this.data_send).subscribe(
            response=>{
              this.msm_success ="Se actualizaron tus datos con éxito";
              
              this._userService.get_users().subscribe(
                response =>{
                  this.usuarios = response.users;
                  console.log(this.usuarios);
                  this.socket.emit("save-users",this.usuarios)
                },error =>{

                }
              )
              
            },error => {

            }
          )
      }
    }
    
  }
  
}
