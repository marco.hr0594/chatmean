# ChatMEAN

Desarrollo MEAN para la construcción de un Chat Web

# Backend - NodeJS - MongoDB - MongoDb Compass
 
*  npm install
*  npm start (config  en páckage.json -> script -> nodemon app.js)
*  puerto 4201

# FrontEnd
 
*  npm install
*  ng serve
*  puerto default de angular
*

# Leer

* Para iniciar en el chat, se debe crear una cuenta (nombre,email, password)
* Con el fin de guardar la conversaci{on en DB y tener detalles de los mismo
* La conversación es  de 1-N



# SERVICIO API

"http://127.0.0.1:4201/api/

# API REST  CHAT

* api.post("/mensaje/enviar", messageController.send);
* api.get("/mensajes/:de/:para", messageController.data_msg);



# API REST USUARIO

* app.post("/registrar",userController.registrar);
* app.post("/login",userController.login);
* app.get("/usuario/:id",userController.get_user);
* app.get("/usuarios",userController.get_users);
* app.put("/usuario/activar/:id",userController.activar_estado);
* app.put("/usuario/desactivar/:id",userController.desactivar_estado);
* app.put("/usuario/editar/imagen/:id",userController.update_image);
* app.get("/usuario/img/:img",userController.get_imagen);
* app.put("/usuario/editar/:id",path,userController.editar_config)
* //total usuarios conectados
* app.get("/getTotalUsers",userController.get_users_online)
