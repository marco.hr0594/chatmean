var User = require("../models/user");
var bcrypt = require("bcrypt-nodejs");
var jwt = require("../helpers/jwt");
var path =  require("path");

function registrar(req,res){
    var params = req.body;
    var user = new User();

    user.nombre = params.nombre;
    user.email = params.email;
    user.imagen = null;
    user.bio = '';
    user.telefono= '';
    user.facebook = 'undefined';
    user.twitter = 'undefined';
    user.estado = false;


    if(params.password){
        bcrypt.hash(params.password,null,null,function(err,hash){
            user.password = hash;
            User.findOne({email:params.email},(err,user_data)=>{
                if(!user_data){
                    user.save((err,user)=>{
                        if(user){
                            res.status(200).send({user:user});
                        }else{
                            res.status(400).send({message:err});
                        }
                    })
                }else{
                    res.status(400).send("El correo ya existe");
                }
            })
        })
        
    }else{
        res.status(500).send("Ingrese la contraseña");
    }
}

function login(req,res){
    var data = req.body;
    User.findOne({email:data.email},(err,user_data) =>{
        if(err){
            res.status(500).send({message:"error en el servidor Login"});
        }else{
            if(!user_data){
                res.status(404).send({message:"El email no esta registrado"});
            }else{
                bcrypt.compare(data.password,user_data.password,function(err,check){
                    if(check){
                        if(data.gettoke){
                            res.status(200).send({
                                jwt : jwt.createToken(user_data),
                                user:user_data,
                                message:"width token"
                            });
                        }else{
                            res.status(200).send({
                                jwt : jwt.createToken(user_data),
                                user:user_data,
                                message:"no token"
                            });
                        }
                    }
                })
            }
        }
    })
}

function get_user(req,res){
    let id = req.params['id'];
    User.findById(id,(err,user)=>{
        if(err){
            res.status(500).send({message:"Error en el servidor Usuario"})
        }else{
            if(user){
                res.status(200).send({user:user});
            }else{
                res.status(500).send({message:"No existe un usuario con ese ID"});
            }
        }
    })
}

function get_users(req,res){
    User.find((err,users)=>{
        if(err){
            res.status(500).send({message:"Error en el servidor Usuarios"})
        }else{
            if(users){
                res.status(200).send({users:users});
            }else{
                res.status(500).send({message:"No existe un usuario con ese ID"});
            }
        } 
    }) 
}

function activar_estado(req,res){
    let id = req.params['id'];
    User.findByIdAndUpdate(id,{estado:true},(err,user_update)=>{
        if(err){
            res.status(500).send({message:"Erron en el servido Des Estatus"})
        }else{
            if(user_update){
                res.status(200).send({user:user_update})
            }else{
                res.status(500).send({message:"No se pudo actualizar el estado"})
            }
        }
    })
}

function desactivar_estado(req,res){
    let id = req.params['id'];
    User.findByIdAndUpdate(id,{estado:false},(err,user_update)=>{
        if(err){
            res.status(500).send({message:"Erron en el servido Des Estatus"})
        }else{
            if(user_update){
                res.status(200).send({user:user_update})
            }else{
                res.status(500).send({message:"No se pudo actualizar el estado"})
            }
        }
    })
}

function update_image(req,res){
    let id = req.params['id'];
    console.log(req); 
    if(req.files.imagen){
        let image_path = req.files.imagen.path;
        let name = image_path.split('\\');
        let image_name = name[2];
        User.findByIdAndUpdate(id,{imagen:image_name},function(err,user_update){
            if(user_update){
                res.status(200).send({user:user_update})
            }else{
                res.status(400).send({message:"No se actualizó la imagen"})
            }
        })
    }else{
        res.status(404).send({message:"No subió la img"})
    }
}

function get_imagen(req,res){
    var img = req.params['img'];

    if(img != "null"){
        var path_img = './uploads/perfiles/'+img;
        res.status(200).sendFile(path.resolve(path_img));
    }else{
        var path_img = './uploads/perfiles/default.png';
        res.status(200).sendFile(path.resolve(path_img));
    }
}

function editar_config(req,res){
    let id = req.params['id'];
    var data = req.body;
    if(req.files.imagen){
        if(data.password){
            bcrypt.hash(data.password,null,null,function(err,hash){
                let imagen_path = req.files.imagen.path;
                let name= imagen_path.split('\\');
                let image_name = name[2];
                if(err){
                    res.status(500).send({message:"HUbo error añ actualizar la contraseña"});
                }else{
                    User.findByIdAndUpdate(id,{
                        nombre:data.nombre,
                        password:hash,
                        imagen:image_name,
                        telefono: data.telefono,
                        bio: data.bio,
                        facebook : data.facebook,
                        twitter : data.twitter,
                        gihub : data.github,
                        estado: data.estado
                         
                    },(err,user_data)=>{
                        if(user_data){
                            res.status(200).send({user:user_data})
                        }
                    })
                    
                }
            })
        }else{
            let imagen_path = req.files.imagen.path;
            let name= imagen_path.split('\\');
            let image_name = name[2];
            User.findByIdAndUpdate(id,{
                nombre:data.nombre,
                imagen:image_name,
                telefono: data.telefono,
                bio: data.bio,
                facebook : data.facebook,
                twitter : data.twitter,
                gihub : data.github,
                estado: data.estado
                 
            },(err,user_data)=>{
                if(user_data){
                    res.status(200).send({user:user_data})
                }
            })

        }
    }else{
        if(data.password){
            console.log("Se mando nueva contraseña");
            bcrypt.hash(data.password,null,null,function(err,hash){
                if(err){
                    res.status(500).send({message:"HUbo error añ actualizar la contraseña"});
                }else{
                    User.findByIdAndUpdate(id,{
                        nombre:data.nombre,
                        password:hash,
                        telefono: data.telefono,
                        bio: data.bio,
                        facebook : data.facebook,
                        twitter : data.twitter,
                        gihub : data.github,
                        estado: data.estado
                         
                    },(err,user_data)=>{
                        if(user_data){
                            res.status(200).send({user:user_data})
                        }
                    })
                    
                }
            })
        }else{
            console.log("Editando info sin contraseña")
            User.findByIdAndUpdate(id,{
                nombre:data.nombre,
                telefono: data.telefono,
                bio: data.bio,
                facebook : data.facebook,
                twitter : data.twitter,
                gihub : data.github,
                estado: data.estado
                 
            },(err,user_data)=>{
                if(user_data){
                    res.status(200).send({user:user_data})
                }
            })
        }
    }
    

}

function get_users_online(req,res){
    User.count({"estado":true},(err,users)=>{
        if(err){
            res.status(500).send({message:"Error en el servidor Usuarios"})
        }else{
            if(users){
                res.status(200).send({total:users});
            }else{
                res.status(500).send({message:"No hay usuarios en linea"});
            }
        } 
    })
}

module.exports = {
    registrar,
    login,
    get_user,
    get_users,
    activar_estado,
    desactivar_estado,
    update_image,
    get_imagen,
    editar_config,
    get_users_online
}