var Message = require("../models/message");

function send (req,res){
    let data = req.body;
    var message = new Message();

    message.de = data.de;
    message.para = data.para;
    message.msg = data.msg;

    message.save((err,message_save)=>{
        if(err){
            res.status(500).send({message:"Error en el servidor al madar msg"})
        }else{
            if(message_save){
                res.status(200).send({message:message_save});
            }
        }
    });
}

function data_msg (req,res){
    var data = req.body;
    var de =  req.params['de'];
    var para = req.params['para'];

    const filtro = {
        '$or':[
            {'$and' : [
                {
                    'para' : de
                },{
                    'de' : para
                }
            ]

            },{
                '$and' : [
                {
                    'para' : para
                },{
                    'de' : de
                }
            ]

            }
        ]
    }

    Message.find(filtro).sort({createAt:1}).exec(function(err,messages){
        if(messages){
            res.status(200).send({message: messages});
        }else{
            res.status(404).send({message:"No hay mensajes entre estos usuarios"})
        }
    });
}


module.exports = {
    send,
    data_msg

}