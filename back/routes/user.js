var expreess = require("express");
var userController = require("../controllers/UserController");
var app = expreess.Router();
var multiparty = require("connect-multiparty");

var path = multiparty({uplodadDir: '/uploads/perfiles'});


app.post("/registrar",userController.registrar);
app.post("/login",userController.login);
app.get("/usuario/:id",userController.get_user);
app.get("/usuarios",userController.get_users);
app.put("/usuario/activar/:id",userController.activar_estado);
app.put("/usuario/desactivar/:id",userController.desactivar_estado);
app.put("/usuario/editar/imagen/:id",userController.update_image);
app.get("/usuario/img/:img",userController.get_imagen);
app.put("/usuario/editar/:id",path,userController.editar_config);
app.get("/getTotalUsers",userController.get_users_online);//total usuarios conectados


module.exports = app;